jQuery(document).ready(function () {
  jQuery(document).on("selectstart", function () {
    return false;
  });
  jQuery("#bg_tab li").click(function () {
    var bg_li_list = jQuery(this).index();
    jQuery(this).addClass("select").siblings().removeClass("select");
    jQuery("#bg_img_box li")
      .removeClass("select")
      .eq(bg_li_list)
      .addClass("select");
  });
  jQuery("#bg_cutting").click(function () {
    if (jQuery("#bg_img").height() == 0) {
      alert("Please upload a picture");
      return false;
    }
    var bg_array = jQuery("#bg_img_region")[0].bg_a;
    if (bg_array == undefined) {
      alert("Please select a region");
      return false;
    }
    var bg_canvas_count = bg_array.length;
    var canvas = document.getElementById("c");
    var ctx = canvas.getContext("2d");
    var img = document.getElementById("bg_img");
    var img_code = jQuery("#code_cut").val();
    if (img_code == "jpg") {
      img_code = "jpeg";
    }
    jQuery("#cut_over ul").html("");
    for (var i = 0; i < bg_canvas_count; i++) {
      canvas.width = bg_array[i][2];
      canvas.height = bg_array[i][3];
      ctx.clearRect(0, 0, bg_array[i][2], bg_array[i][3]);
      ctx.fillStyle = "#ffffff";
      ctx.fillRect(0, 0, bg_array[i][2], bg_array[i][3]);
      ctx.drawImage(img, -bg_array[i][0], -bg_array[i][1]);
      jQuery("#cut_over ul").append(
        "<li style='top:" +
          bg_array[i][1] +
          "px;left:" +
          bg_array[i][0] +
          "px;'><a href='#none'><img src='" +
          canvas.toDataURL("image/" + img_code) +
          "' /></a><span>" +
          (i + 1) +
          "</span></li>"
      );
    }
    jQuery("#cut_over").height(jQuery("#bg_img").height());
    jQuery("#bg_tab li").eq(1).click();
  });
  jQuery("#bg_download").click(function () {
    if (jQuery("#cut_over img").length == 0) {
      alert("没有图片可以下载");
      return false;
    }
    jQuery("#cut_over a").each(function () {
      jQuery(this).attr("href", jQuery(this).find("img").attr("src"));
      jQuery(this).attr(
        "download",
        jQuery("#img_name").val() + "" + (jQuery(this).parent().index() + 1)
      );
    });
    var bg_arr = jQuery("#cut_over a");
    var bg_a_length = jQuery("#cut_over a").length;
    var bg_a = 0;
    function bg_xia() {
      if (bg_a < bg_a_length) {
        bg_arr.eq(bg_a).find("img").click();
        setTimeout(bg_xia, 500);
        bg_a++;
      } else {
        bg_arr.removeAttr("download");
        bg_arr.attr("href", "#none");
      }
    }
    bg_xia();
  });
  jQuery("#bg_img_clear").click(function () {
    jQuery("#cut_over ul > li").remove();
  });
  jQuery(document).keydown(function (key) {
    if (key.keyCode == 13) {
      if (jQuery("#num_cut").is(":focus") || jQuery("#hei_cut").is(":focus")) {
        jQuery("#bg_cutting").click();
      } else if (jQuery("#img_name").is(":focus")) {
        jQuery("#bg_download").click();
      }
    }
  });
});
function previewFile() {
  jQuery("#bg_canvas_box").html(
    '<div id="bg_img_region"><img id="bg_img" /></div>'
  );
  var preview = document.getElementById("bg_img");
  var file = document.getElementById("bg_file").files[0];
  var reader = new FileReader();
  reader.onloadend = function () {
    preview.src = reader.result;
  };
  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
  jQuery(".bg_tab_list").eq(0).click();
  jQuery(preview).load(function () {
    jQuery("#bg_img_region").bg_region({
      cut: "#num_cut",
      hei: "#hei_cut",
      x_cut: "#num_cut2",
      wid: "#wid_cut",
      clear: "#bg_img_clear",
    });
  });
}
